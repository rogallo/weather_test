package com.weather.weathertest

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.weather.weathertest.main.view.MainActivity
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class SearchFragmentTest {

    @Test
    fun test_textInputLayoutIsVisible() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.search_input_layout)).check(matches(isDisplayed()))
    }

    @Test
    fun test_textInputEditTextIsVisible() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.search_input_edit_text)).check(matches(isDisplayed()))
    }

    @Test
    fun test_recyclerViewIsVisible() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.search_recyclerview)).check(matches(isDisplayed()))
    }

    @Test
    fun test_hideKeyboardAfterPressBackAction() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.search_input_layout)).perform(click())
        pressBack()
        closeSoftKeyboard()
    }
}