package com.weather.weathertest

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.weather.weathertest.details.view.DetailsFragment
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class DetailsFragmentTest {

    @Test
    fun test_detailsTitleIsVisible() {
        launchFragmentInContainer<DetailsFragment>()
        onView(withId(R.id.details_title_text_view)).check(matches(isDisplayed()))
    }

    @Test
    fun test_detailsTemperatureIsVisible() {
        launchFragmentInContainer<DetailsFragment>()
        onView(withId(R.id.details_temperature_text_view)).check(matches(isDisplayed()))
    }

    @Test
    fun test_detailsNextHoursIsVisible() {
        launchFragmentInContainer<DetailsFragment>()
        onView(withId(R.id.details_next_hours_text_view)).check(matches(isDisplayed()))
    }

    @Test
    fun test_detailsTitleTextIsCorrect() {
        launchFragmentInContainer<DetailsFragment>()
        onView(withId(R.id.details_title_text_view)).check(matches(withText(R.string.details_today_title)))
    }

    @Test
    fun test_detailsNextHoursIsCorrect() {
        launchFragmentInContainer<DetailsFragment>()
        onView(withId(R.id.details_next_hours_text_view)).check(matches(withText(R.string.details_next_hours_title)))
    }
}