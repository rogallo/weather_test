package com.weather.weathertest

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    SearchFragmentTest::class,
    DetailsFragmentTest::class
)
class FragmentTestSuite