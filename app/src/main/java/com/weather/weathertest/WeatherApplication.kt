package com.weather.weathertest

import android.app.Application
import com.weather.weathertest.di.networkModule
import com.weather.weathertest.di.repositoryModule
import com.weather.weathertest.di.systemModule
import com.weather.weathertest.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class WeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@WeatherApplication)
            modules(listOf(networkModule, repositoryModule, viewModelModule, systemModule))
        }
    }
}