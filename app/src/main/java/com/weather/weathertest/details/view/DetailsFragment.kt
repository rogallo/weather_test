package com.weather.weathertest.details.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.weather.weathertest.R
import com.weather.weathertest.databinding.FragmentDetailsBinding
import com.weather.weathertest.details.adapter.DetailsForecastAdapter
import com.weather.weathertest.details.viewmodel.DetailsViewModel
import com.weather.weathertest.utils.showToast
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_details.view.details_recyclerview as detailsList

class DetailsFragment : Fragment() {

    private val detailsViewModel: DetailsViewModel by viewModel()
    private lateinit var viewDataBinding: ViewDataBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        FragmentDetailsBinding.inflate(inflater, container, false).apply {
            viewDataBinding = this
            vm = detailsViewModel
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initObservers()
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        detailsViewModel.run {
            getForecastWeather(arguments?.getString(KEY_CITY_CODE))
            setCityData(arguments?.getString(KEY_CITY_NAME))
        }
    }

    private fun initView(view: View) {
        view.detailsList.adapter = DetailsForecastAdapter()
    }

    private fun initObservers() {
        detailsViewModel.exceptionAction = { context?.showToast(getString(R.string.network_connection)) }
    }
}

const val KEY_CITY_CODE = "KEY_CITY_CODE"
const val KEY_CITY_NAME = "KEY_CITY_NAME"