package com.weather.weathertest.details.model

import com.weather.weathertest.network.API_KEY
import com.weather.weathertest.network.NetworkResult
import com.weather.weathertest.network.WeatherApi
import com.weather.weathertest.network.model.ForecastModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import java.util.*

class DetailsRepository(private val weatherApi: WeatherApi) {

    suspend fun fetchForecastForCityAsync(cityKey: String?) : Deferred<NetworkResult<LinkedList<ForecastModel>>> {
        return withContext(Dispatchers.IO) {
            async {
                try {
                    weatherApi.getForecastWeather(cityKey!!, API_KEY, details = true, metric = true).await().run {
                        if (isSuccessful) {
                            NetworkResult.Success(body())
//                            Resource.success(body() as LinkedList<ForecastModel>)
                        } else {
                            NetworkResult.Error(message())
//                            Resource.error(message(), null)
                        }
                    }
                } catch (exception: Exception) {
                    NetworkResult.Error("")
                }
            }
        }
    }
}