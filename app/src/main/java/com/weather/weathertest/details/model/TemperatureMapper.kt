package com.weather.weathertest.details.model

import com.weather.weathertest.R

fun Float.mapTemperatureToColor() =
    when {
        this < 10.0f -> R.color.colorBlue
        this in 10.0f..20.0f -> R.color.colorBlack
        this > 20.0f -> R.color.colorRed
        else -> R.color.colorBlack
    }