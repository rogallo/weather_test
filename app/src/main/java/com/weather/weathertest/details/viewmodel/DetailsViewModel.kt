package com.weather.weathertest.details.viewmodel

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.weather.weathertest.R
import com.weather.weathertest.details.model.DetailsRepository
import com.weather.weathertest.details.model.mapTemperatureToColor
import com.weather.weathertest.main.base.BaseViewModel
import com.weather.weathertest.network.NetworkResult
import com.weather.weathertest.network.model.ForecastModel
import com.weather.weathertest.utils.manager.NetworkManager
import kotlinx.coroutines.launch
import java.util.*

class DetailsViewModel constructor(
    private val detailsRepository: DetailsRepository,
    private val resources: Resources,
    private val networkManager: NetworkManager) : BaseViewModel() {

    val forecastLiveData = MutableLiveData<LinkedList<ForecastModel>>()
    val cityLiveData = MutableLiveData<String>()
    val temperatureLiveData = Transformations.map(forecastLiveData) {
        if (!it.isNullOrEmpty()) it[0].temperature.value.toString() else noData
    }
    val temperatureColorLiveData = Transformations.map(forecastLiveData) {
        resources.getColor(if (!it.isNullOrEmpty()) it[0].temperature.value.mapTemperatureToColor() else R.color.colorBlack, null)
    }
    val descriptionLiveData = Transformations.map(forecastLiveData) {
        if (!it.isNullOrEmpty()) it[0].iconPhrase else noData
    }
    val windLiveData = Transformations.map(forecastLiveData) {
        if (!it.isNullOrEmpty()) resources.getString(R.string.details_wind_format, it[0].wind.speed.value.toString()) else noData
    }
    val rainLiveData = Transformations.map(forecastLiveData) {
        if (!it.isNullOrEmpty()) resources.getString(R.string.details_rain_format, it[0].rain.value.toString()) else noData
    }

    private val noData = resources.getString(R.string.no_data)

    fun getForecastWeather(cityKey: String?) {
        if (networkManager.isNetworkAvailable()) {
            fetchForecastDetails(cityKey)
        } else {
            exceptionAction(resources.getString(R.string.network_connection))
        }
    }

    private fun fetchForecastDetails(cityKey: String?) {
        viewModelScope.launch {
            detailsRepository.fetchForecastForCityAsync(cityKey).await().run {
                when(this) {
                    is NetworkResult.Success -> { forecastLiveData.value = data }
                    is NetworkResult.Error -> { exceptionAction(message!!) }
                }
            }
        }
    }

    fun setCityData(cityName: String?) {
        cityLiveData.run {
            if (value != cityName) {
                value = cityName
            }
        }
    }
}