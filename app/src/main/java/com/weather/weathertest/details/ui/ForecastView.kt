package com.weather.weathertest.details.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.cardview.widget.CardView
import com.weather.weathertest.R
import kotlinx.android.synthetic.main.view_forecast.view.forecast_description_text_view as descriptionText
import kotlinx.android.synthetic.main.view_forecast.view.forecast_hour_text_view as hourText
import kotlinx.android.synthetic.main.view_forecast.view.forecast_temperature_text_view as temperatureText

class ForecastView : CardView {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyle: Int) : super(context, attributeSet, defStyle) {
        LayoutInflater.from(context).inflate(R.layout.view_forecast, this)
    }

    fun setHour(hour: String) {
        hourText.text = hour
    }

    fun setTemperature(temperature: String) {
        temperatureText.text = temperature
    }

    fun setDescription(description: String) {
        descriptionText.text = description
    }
}