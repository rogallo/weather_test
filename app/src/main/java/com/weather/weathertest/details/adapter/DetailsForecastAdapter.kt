package com.weather.weathertest.details.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.weather.weathertest.R
import com.weather.weathertest.details.adapter.viewholder.DetailsForecastViewHolder
import com.weather.weathertest.network.model.ForecastModel
import java.util.*

class DetailsForecastAdapter : RecyclerView.Adapter<DetailsForecastViewHolder>() {

    val items = LinkedList<ForecastModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DetailsForecastViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.view_item_forecast, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: DetailsForecastViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setItems(forecastList: List<ForecastModel>) {
        items.run {
            clear()
            addAll(forecastList)
        }
        notifyDataSetChanged()
    }
}

@BindingAdapter("forecastData")
fun setForecastData(recyclerView: RecyclerView, data: List<ForecastModel>?) {
    (recyclerView.adapter as DetailsForecastAdapter).run { data?.run { setItems(this) } }
}