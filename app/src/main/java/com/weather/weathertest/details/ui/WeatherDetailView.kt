package com.weather.weathertest.details.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import com.weather.weathertest.R
import kotlinx.android.synthetic.main.view_weather_detail.view.weather_image_view as weatherImage
import kotlinx.android.synthetic.main.view_weather_detail.view.weather_title_text_view as weatherTitle
import kotlinx.android.synthetic.main.view_weather_detail.view.weather_value_text_view as weatherData

class WeatherDetailView : LinearLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyle: Int) : super(context, attributeSet, defStyle) {
        LayoutInflater.from(context).inflate(R.layout.view_weather_detail, this)
        context.theme.obtainStyledAttributes(attributeSet, R.styleable.WeatherDetailViewStyle, 0, 0).apply {
            weatherImage.setImageDrawable(getDrawable(R.styleable.WeatherDetailViewStyle_image))
            weatherTitle.text = getString(R.styleable.WeatherDetailViewStyle_title)
            recycle()
        }
    }

    fun setDataValue(dataValue: String) {
        weatherData.text = dataValue
    }
}

@BindingAdapter("weatherDataValue")
fun setWeatherDataValue(weatherDetailView: WeatherDetailView, value: String?) {
    value?.run { weatherDetailView.setDataValue(this) }
}