package com.weather.weathertest.details.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.weather.weathertest.R
import com.weather.weathertest.network.model.ForecastModel
import com.weather.weathertest.utils.convertToHour
import kotlinx.android.synthetic.main.view_item_forecast.view.forecast_view as forecastView

class DetailsForecastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(forecastModel: ForecastModel) = with(forecastModel) {
        itemView.forecastView.run {
            setHour(epochDateTime.convertToHour().toString())
            setTemperature(itemView.resources.getString(R.string.details_temperature_format, temperature.value.toString()))
            setDescription(iconPhrase)
        }
    }
}