package com.weather.weathertest.utils

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("inputError")
fun setInputError(textInputLayout: TextInputLayout, error: String?) {
    textInputLayout.error = error
}