package com.weather.weathertest.utils

import java.text.SimpleDateFormat
import java.util.*

fun Long.convertToHour(): String {
    val date = Date(this * 1000L)
    val formatter = SimpleDateFormat("HH:mm").apply {
        timeZone = TimeZone.getTimeZone("Etc/UTC") }
    return formatter.format(date)
}