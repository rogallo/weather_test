package com.weather.weathertest.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.weather.weathertest.R
import com.weather.weathertest.network.model.CityModel
import com.weather.weathertest.search.adapter.viewholder.SearchCityViewHolder
import java.util.*

class SearchCityAdapter : RecyclerView.Adapter<SearchCityViewHolder>() {

    private val items = LinkedList<CityModel>()
    var clickAction: (String, String) -> Unit = { s: String, s1: String -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SearchCityViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.view_item_city, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: SearchCityViewHolder, position: Int) {
        holder.run {
            bind(items[position])
            itemView.setOnClickListener {
                clickAction(items[position].key, items[position].localizedName) }
        }
    }

    fun setItems(cityList: List<CityModel>) {
        items.run {
            clear()
            addAll(cityList)
        }
        notifyDataSetChanged()
    }
}

@BindingAdapter("cityData")
fun setCityData(recyclerView: RecyclerView, data: List<CityModel>?) {
    (recyclerView.adapter as SearchCityAdapter).run { data?.run { setItems(this) } }
}