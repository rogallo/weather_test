package com.weather.weathertest.search.model

import com.weather.weathertest.network.API_KEY
import com.weather.weathertest.network.Resource
import com.weather.weathertest.network.WeatherApi
import com.weather.weathertest.network.model.CityModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import java.util.*

class SearchRepository(private val weatherApi: WeatherApi) {

    suspend fun fetchSearchCityListAsync(cityName: String) : Deferred<Resource<LinkedList<CityModel>>> {
        return withContext(Dispatchers.IO) {
            async {
                try {
                    weatherApi.getCityList(API_KEY, cityName).await().run {
                        if (isSuccessful) {
                            Resource.success(body() as LinkedList<CityModel>)
                        } else {
                            Resource.error(message(), null)
                        }
                    }
                }
                catch (exception: Exception) {
                    Resource.error("Unknown error", null)
                }
            }
        }
    }
}