package com.weather.weathertest.search.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.weather.weathertest.network.model.CityModel
import kotlinx.android.synthetic.main.view_item_city.view.view_search_city as cityText
import kotlinx.android.synthetic.main.view_item_city.view.view_search_country as countryText

class SearchCityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(cityModel: CityModel) = with(cityModel) {
        itemView.run {
            cityText.text = localizedName
            countryText.text = country.localizedName
        }
    }
}