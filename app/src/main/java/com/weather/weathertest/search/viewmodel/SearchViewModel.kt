package com.weather.weathertest.search.viewmodel

import android.content.res.Resources
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.weather.weathertest.R
import com.weather.weathertest.main.base.BaseViewModel
import com.weather.weathertest.network.model.CityModel
import com.weather.weathertest.network.model.Status.ERROR
import com.weather.weathertest.network.model.Status.SUCCESS
import com.weather.weathertest.search.model.ErrorData.ERROR_REGULAR_EXPRESSION
import com.weather.weathertest.search.model.ErrorData.ERROR_TOO_SHORT
import com.weather.weathertest.search.model.SearchRepository
import com.weather.weathertest.utils.MIN_LENGTH_FOR_CITY_NAME
import com.weather.weathertest.utils.manager.NetworkManager
import com.weather.weathertest.utils.validateRegularExpression
import kotlinx.coroutines.launch
import java.util.*

class SearchViewModel constructor(
    private val searchRepository: SearchRepository,
    private val resources: Resources,
    private val networkManager: NetworkManager) : BaseViewModel() {

    val validateErrorLiveData = MutableLiveData<String>()
    val cityListLiveData = MutableLiveData<LinkedList<CityModel>>()
    val progressVisibilityLiveData = MutableLiveData<Int>(View.GONE)
    var hideKeyboardAction: () -> Unit = {}

    fun validateCityName(cityName: String) {
        cityName.run {
            when {
                length < MIN_LENGTH_FOR_CITY_NAME -> {
                    validateErrorLiveData.value = resources.getString(ERROR_TOO_SHORT.text)
                }
                validateRegularExpression() -> {
                    validateErrorLiveData.value = resources.getString(ERROR_REGULAR_EXPRESSION.text)
                }
                else -> {
                    validateErrorLiveData.value = null
                    getSearchCityList(this)
                    hideKeyboardAction()
                }
            }
        }
    }

    private fun getSearchCityList(cityName: String) {
        if (networkManager.isNetworkAvailable()) {
            changeProgressVisibility(View.VISIBLE)
            fetchCityList(cityName)
        } else {
            exceptionAction(resources.getString(R.string.network_connection))
        }
    }

    private fun fetchCityList(cityName: String) {
        viewModelScope.launch {
            searchRepository.fetchSearchCityListAsync(cityName).await().run {
                when(status) {
                    SUCCESS -> {
                        cityListLiveData.value = data
                        changeProgressVisibility(View.GONE) }
                    ERROR -> {
                        exceptionAction(message!!)
                        changeProgressVisibility(View.GONE) }
                }
            }
        }
    }

    private fun changeProgressVisibility(visibility: Int) {
        progressVisibilityLiveData.run {
            if (value != visibility) {
                value = visibility
            }
        }
    }
}