package com.weather.weathertest.search.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.os.bundleOf
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.weather.weathertest.R
import com.weather.weathertest.databinding.FragmentSearchBinding
import com.weather.weathertest.details.view.KEY_CITY_CODE
import com.weather.weathertest.details.view.KEY_CITY_NAME
import com.weather.weathertest.search.adapter.SearchCityAdapter
import com.weather.weathertest.search.model.CityNameProvider
import com.weather.weathertest.search.viewmodel.SearchViewModel
import com.weather.weathertest.utils.hideKeyboard
import com.weather.weathertest.utils.showToast
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_search.view.search_input_layout as searchInput
import kotlinx.android.synthetic.main.fragment_search.view.search_recyclerview as searchList

class SearchFragment : Fragment(), CityNameProvider {

    private val searchViewModel: SearchViewModel by viewModel()
    private val searchCityAdapter: SearchCityAdapter by lazy { SearchCityAdapter() }
    private lateinit var viewDataBinding: ViewDataBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        FragmentSearchBinding.inflate(inflater, container, false).apply {
            viewDataBinding = this
            vm = searchViewModel
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initObservers()
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
    }

    override fun provideCityName() = view?.searchInput?.editText?.text.toString()

    private fun initView(view: View) {
        view.run {
            navController = Navigation.findNavController(this)
            searchList.adapter = searchCityAdapter
            initSearchInputView(view)
        }
    }

    private fun initSearchInputView(view: View) {
        view.searchInput.run {
            setEndIconOnClickListener { searchViewModel.validateCityName(provideCityName()) }
            editText?.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchViewModel.validateCityName(provideCityName())
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun initObservers() {
        searchCityAdapter.clickAction = { cityCode: String, cityName: String ->
            navController.navigate(R.id.action_searchFragment_to_detailsFragment,
                bundleOf(KEY_CITY_CODE to cityCode, KEY_CITY_NAME to cityName))
        }
        searchViewModel.run {
            hideKeyboardAction = { hideKeyboard() }
            exceptionAction = { context?.showToast(it) }
        }
    }
}