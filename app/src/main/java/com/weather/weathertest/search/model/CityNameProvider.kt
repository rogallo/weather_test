package com.weather.weathertest.search.model

interface CityNameProvider {

    fun provideCityName(): String?
}