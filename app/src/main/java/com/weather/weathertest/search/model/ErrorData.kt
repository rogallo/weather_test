package com.weather.weathertest.search.model

import androidx.annotation.StringRes
import com.weather.weathertest.R

enum class ErrorData(@StringRes val text: Int) {
    ERROR_TOO_SHORT(R.string.search_error_too_short),
    ERROR_REGULAR_EXPRESSION(R.string.search_error_regular_expression)
}