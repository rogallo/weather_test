package com.weather.weathertest.di

import com.weather.weathertest.details.model.DetailsRepository
import com.weather.weathertest.search.model.SearchRepository
import org.koin.dsl.module

val repositoryModule = module {

    single {
        SearchRepository(get())
    }

    single {
        DetailsRepository(get())
    }
}