package com.weather.weathertest.di

import com.weather.weathertest.details.viewmodel.DetailsViewModel
import com.weather.weathertest.search.viewmodel.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { SearchViewModel(get(), get(), get()) }

    viewModel { DetailsViewModel(get(), get(), get()) }
}