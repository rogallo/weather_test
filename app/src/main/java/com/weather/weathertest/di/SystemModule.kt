package com.weather.weathertest.di

import android.content.Context
import android.net.ConnectivityManager
import com.weather.weathertest.utils.manager.NetworkManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val systemModule = module {

    single { androidContext().resources }

    single { androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    single { NetworkManager(get()) }
}