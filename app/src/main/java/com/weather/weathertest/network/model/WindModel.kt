package com.weather.weathertest.network.model

import com.google.gson.annotations.SerializedName
import com.weather.weathertest.network.SPEED

data class WindModel(
    @SerializedName(SPEED) val speed: SpeedModel
)