package com.weather.weathertest.network.model

import com.google.gson.annotations.SerializedName
import com.weather.weathertest.network.ID
import com.weather.weathertest.network.LOCALIZED_NAME

data class CountryModel(
    @SerializedName(ID) val id: String,
    @SerializedName(LOCALIZED_NAME) val localizedName: String
)