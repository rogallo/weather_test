package com.weather.weathertest.network.model

enum class Status {
    SUCCESS,
    ERROR
}