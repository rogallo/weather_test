package com.weather.weathertest.network

import com.weather.weathertest.network.model.CityModel
import com.weather.weathertest.network.model.ForecastModel
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

interface WeatherApi {

    @GET("/locations/v1/cities/search")
    fun getCityList(@Query("apikey") apiKey: String, @Query("q") city: String):
            Deferred<Response<LinkedList<CityModel>>>

    @GET("/forecasts/v1/hourly/12hour/{cityKey}")
    fun getForecastWeather(@Path("cityKey") city: String,
                           @Query("apikey") apiKey: String,
                           @Query("details") details: Boolean,
                           @Query("metric") metric: Boolean):
            Deferred<Response<LinkedList<ForecastModel>>>
}