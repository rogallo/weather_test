package com.weather.weathertest.network.model

import com.google.gson.annotations.SerializedName
import com.weather.weathertest.network.*

data class CityModel(
    @SerializedName(VERSION) val version: Int,
    @SerializedName(KEY) val key: String,
    @SerializedName(TYPE) val type: String,
    @SerializedName(RANK) val rank: Int,
    @SerializedName(LOCALIZED_NAME) val localizedName: String,
    @SerializedName(COUNTRY) val country: CountryModel,
    @SerializedName(ADMINISTRATIVE_AREA) val administrativeArea: AdministrativeAreaModel
)