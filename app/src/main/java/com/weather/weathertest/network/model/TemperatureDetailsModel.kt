package com.weather.weathertest.network.model

import com.google.gson.annotations.SerializedName
import com.weather.weathertest.network.VALUE

data class TemperatureDetailsModel(
    @SerializedName(VALUE) val value: Float
)