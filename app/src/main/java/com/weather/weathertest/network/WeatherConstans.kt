package com.weather.weathertest.network

const val BASE_URL = "http://dataservice.accuweather.com"
const val API_KEY = "1GxaIgQpvyQnCUxPGGixoV3V119ktplj"

// City Model
const val VERSION = "Version"
const val KEY = "Key"
const val TYPE = "Type"
const val RANK = "Rank"
const val COUNTRY = "Country"
const val ADMINISTRATIVE_AREA = "AdministrativeArea"

// Forecast Model
const val EPOCH_DATE_TIME = "EpochDateTime"
const val ICON_PHRASE = "IconPhrase"
const val TEMPERATURE = "Temperature"
const val WIND = "Wind"
const val RAIN = "Rain"

// Other field
const val ID = "ID"
const val LOCALIZED_NAME = "LocalizedName"
const val VALUE = "Value"
const val SPEED = "Speed"