package com.weather.weathertest.network

import com.weather.weathertest.network.model.Status
import com.weather.weathertest.network.model.Status.ERROR
import com.weather.weathertest.network.model.Status.SUCCESS

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(ERROR, data, msg)
        }
    }
}