package com.weather.weathertest.network.model

import com.google.gson.annotations.SerializedName
import com.weather.weathertest.network.*

data class ForecastModel(
    @SerializedName(EPOCH_DATE_TIME) val epochDateTime: Long,
    @SerializedName(ICON_PHRASE) val iconPhrase: String,
    @SerializedName(TEMPERATURE) val temperature: TemperatureDetailsModel,
    @SerializedName(WIND) val wind: WindModel,
    @SerializedName(RAIN) val rain: RainModel
)