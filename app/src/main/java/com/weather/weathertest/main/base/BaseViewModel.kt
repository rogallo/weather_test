package com.weather.weathertest.main.base

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    var exceptionAction: (String) -> Unit = {}
}