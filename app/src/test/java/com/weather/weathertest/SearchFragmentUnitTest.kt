package com.weather.weathertest

import com.weather.weathertest.utils.validateRegularExpression
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test

class SearchFragmentUnitTest {

    @Test
    fun test_validationCityNameIsNotRegularExpression() {
        assertTrue("Warsaw5555".validateRegularExpression())
    }

    @Test
    fun test_validationCityNameIsRegularExpression() {
        assertFalse("Warsaw".validateRegularExpression())
    }
}