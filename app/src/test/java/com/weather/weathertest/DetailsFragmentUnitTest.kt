package com.weather.weathertest

import com.weather.weathertest.utils.convertToHour
import junit.framework.Assert.assertEquals
import org.junit.Test

class DetailsFragmentUnitTest {

    @Test
    fun test_validationConvertEpochTimeToHours() {
        assertEquals(1595930400L.convertToHour(), "10:00")
    }
}